
package Project;
import javax.swing.*;
import java.awt.*;

/* An abstract class to build graphs with */

public abstract class Graph extends  JPanel{
    String startDate;
    String endDate;
    CSVReader csvRead = new CSVReader("C:\\Users\\Guy Clark\\IdeaProjects\\TestingGraphs\\src\\gridwatch.csv");

    public Graph(String startDate, String endDate){
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public abstract void Draw(Graphics g);

    @Override
    public void paintComponent(Graphics g){
        Draw(g);
    }
}
