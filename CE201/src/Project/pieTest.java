package Project;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import javax.swing.JPanel;

import javax.swing.JComponent;
import javax.swing.JFrame;

/*
    Slice class used to model an object of a slice in a pie chart.
    Takes in a value and a colour for when the slice is created
    in the graph
*/
class Slice {
    double value;
    Color color;

    public Slice(double value, Color color) {
        this.value = value;
        this.color = color;
    }
    public Slice(){}
}
/*
    Pie Graph class used to generate a pie chart graph on the app
    Extends JPanel and behaves like one, but has custom paintComponent()
    method that draws the graph to the screen
*/
class PieGraph extends JPanel {
    // Variables needed for the graph to work with the CSVReader
    String startDate;
    String endDate;
    ArrayList<String> energyValues;
    double[] values;
    Slice[] slices;

    public PieGraph(String startDate, String endDate, ArrayList<String> energyValues,
                    double[] currentValues) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.energyValues = energyValues;
        this.values = currentValues;
        this.slices = new Slice[energyValues.size()];
        // Call the method to create slices
        makeSlices(this.slices);

    }

    // Method used to match the pie slices to energy values
    private void makeSlices(Slice[] sArray){
        for(int i=0; i<sArray.length;i++){
            int red = (int)(Math.random() * 256);
            int green =(int) (Math.random() * 256);
            int blue = (int) (Math.random() * 256);
            sArray[i] = new Slice();
            sArray[i].value = values[i];
            sArray[i].color = new Color(red,green,blue);
        }
    }

    /*
        Override the paintComponent method for custom painting behaviour
        This method originally called "paint()", but that is reserved for
        AWT containers like Frame, Window and sorts. Swing components must use
        the paintComponent method for both initial drawing and repainting
    */
    @Override
    public void paintComponent(Graphics g){
        drawPie((Graphics2D) g, getBounds(), slices);
    }
    void drawPie(Graphics2D g, Rectangle area, Slice[] slices) {
        int initalYCord = 40;
        System.out.println("Area is: " + area.getBounds());
        double total = 0.0D;
        for (int i = 0; i < slices.length; i++) {
            total += slices[i].value;
        }

        double curValue = 0.0D;
        int startAngle = 0;
        for (int i = 0; i < slices.length; i++) {
            startAngle = (int) (curValue * 360 / total);
            int arcAngle = (int) (slices[i].value * 360 / total);

            g.setColor(slices[i].color);
            g.fillArc(area.x, area.y, area.width-250, area.height-50, startAngle, arcAngle);
            curValue += slices[i].value;
        }
        // Print the key with the values, their total and a colour code
        g.setFont(new Font("TimesRoman", Font.PLAIN, 15));
        for(int i=0;i<energyValues.size();i++){
            g.setColor(getEnergyTypeColor(energyValues.get(i)));
            g.drawString(energyValues.get(i) + ": " + values[i] ,100,initalYCord);
            initalYCord += 20;
        }

    }
    /*
        Method used to get the colour key of the pie graph
        Asks for a string that identifies the energyType
        Returns a color object with the correct colour
    */
    private Color getEnergyTypeColor(String energyType){
        String[] refValues = new String[]{ "coal", "nuclear", "ccgt", "wind", "french_ict" ,"dutch_ict",
                "irish_ict", "ew_ict", "pumped", " hydro", "oil", "ocgt", "other", "solar"};
        if(energyType.equals(refValues[0])) return Color.black;
        else if(energyType.equals(refValues[1])) return new Color(0,204,0);
                else if(energyType.equals(refValues[2])) return Color.BLUE;
                else if(energyType.equals(refValues[3])) return Color.lightGray;
                else if(energyType.equals(refValues[4])) return Color.red;
        // Still need to add more colours to each energy type
        // For now I just make random colours until the other colours have been implemented
        else{
            //System.out.println(energyType + " Was generated with random colour");
            int red = (int)(Math.random() * 256);
            int green =(int) (Math.random() * 256);
            int blue = (int) (Math.random() * 256);
            return new Color(red,green,blue);
        }
    }
}

