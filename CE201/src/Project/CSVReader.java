
package Project;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
/*
    CSVReader.java - Created by Guy Clark
    Contributors - Guy Clark
*/
public class CSVReader {

    FileReader fileRead;
    BufferedReader buffRead;
    String fileName;

    public CSVReader(String fileName){

        this.fileName = fileName;
    }


    // Method used to get the total GW values of ta
    public double[] getTotals(ArrayList<String> energyTypes, String startDate, String endDate){

        double[] total = new double[energyTypes.size()];
        String line;
        String dataSet = "";
        boolean check = false;
        try {
            for(int i=0;i<energyTypes.size();i++){

                int section = getSection(energyTypes.get(i));
                fileRead = new FileReader(fileName);
                buffRead = new BufferedReader(fileRead);

                while((line = buffRead.readLine()) !=null){

                    String[] currentLine = line.split(",");
                    if(currentLine[1].contains(endDate)){
                        System.out.println("Stopped at date - " + currentLine[1]);
                        break;
                    }
                    else if(currentLine[1].contains(startDate)){
                        if(!currentLine[section].equals(" 0")){
                            dataSet = (currentLine[section]);
                            check = true;
                        }
                    }

                    if(check == true) total[i]+= returnValue(dataSet);
                }
                System.out.println("Added values for type: " + energyTypes.get(i));
            }
        }
        catch(FileNotFoundException e ){
            System.out.println("ERROR - FILE DOESN'T EXIST");
            System.out.println(e.getMessage());
        }
        catch(IOException e){
            System.out.println("ERROR - IO EXCEPTION FOUND");
            System.out.println(e.getMessage());
        }
        // At the end of the method make sure all the streams are closed
        // This is for performance reasons
        finally {
            try {
                buffRead.close();
                fileRead.close();
            }catch (IOException e){
                System.out.println("FAILED TO CLOSE STREAMS");
                System.out.println(e.getMessage());
            }

            for(int i=0;i<total.length;i++){
                System.out.println("Total for item - " + i + "was: " + total[i] );
                total[i] /= 100000;
                System.out.println("Total for item - " + i + " now is: " + total[i] );
            }

            return total;
        }

    }
    /*
        Function used to get the average GW's of energy types
        Needs a start date, an end date and an array list of energy types
        Returns an integer array that contain the average values
    */
    public double[] getAverage(ArrayList<String> energyTypes, String startDate,String endDate){

        String line;
        String dataSet = "";
        boolean check = false;
        double[] averages = new double[energyTypes.size()];

        for(int i=0;i<energyTypes.size();i++){
            int count = 0;
            try{
                fileRead = new FileReader(fileName);
                buffRead = new BufferedReader(fileRead);
            } catch(FileNotFoundException e) {
                System.out.println("ERROR - FILE DOESN'T EXIST");
                System.out.println(e.getMessage());
            }

            int section = getSection(energyTypes.get(i));
            try {
                while ((line = buffRead.readLine()) != null) {
                    String[] currentLine = line.split(",");
                    if(currentLine[1].contains(endDate)) break;
                    else if(currentLine[1].contains(startDate)){
                        dataSet = currentLine[section];
                        count ++;
                        check = true;
                    }
                    if(check){
                        averages[i] += returnValue(dataSet);
                        check = false;
                    }

                }
                double temp = averages[i];
                System.out.println("Working on energy type - " + energyTypes.get(i));
                System.out.println(averages[i]);
                temp = temp / count;
                temp = temp / 1000;
                averages[i] = temp;
                System.out.println(averages[i] + " after average");

            }catch (IOException e) {
                System.out.println("ERROR - IO EXCEPTION FOUND");
                System.out.println(e.getMessage());
            }

        }
        return averages;
    }

    // Used to detect what column (section) the energy type is located at
    public int getSection(String dataType){
        if(dataType.equals("coal")) return 4;
        else if(dataType.equals("nuclear")) return 5;
        else if(dataType.equals("ccgt")) return 6;
        else if(dataType.equals("wind")) return 7;
        else if(dataType.equals("french_ict")) return 8;
        else if(dataType.equals("dutch_ict")) return 9;
        else if(dataType.equals("irish_ict")) return 10;
        else if(dataType.equals("ew_ict")) return 11;
        else if(dataType.equals("pumped")) return 12;
        else if(dataType.equals("hydro")) return 13;
        else if(dataType.equals("oil")) return 14;
        else if(dataType.equals("ocgt")) return 15;
        else if(dataType.equals("other")) return 16;
        else if(dataType.equals("solar")) return 17;
        else return 0;
    }

    // Used to remove any whitespace in values and returns an int
    // Sometimes when reading from a CSV file the value with have a space at the beginning
    private Double returnValue(String dataSet){
        String v = dataSet.replaceAll(" ","");
        return Double.parseDouble(v);

    }
}
