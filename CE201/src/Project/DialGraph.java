package Project;

import com.sun.prism.Graphics;

import javax.swing.*;
import java.awt.*;

/**
 * Created by gc16077 on 21/02/2018.
 */
public class DialGraph extends JPanel{

    int freq;
    int demand;

    public DialGraph(int freq, int demand){
        this.freq=freq;
        this.demand = demand;
    }

    @Override
    public void paintComponent(java.awt.Graphics g){
        java.awt.Graphics g2d = (Graphics2D)g;
        BasicStroke bs = new BasicStroke(5);
        ((Graphics2D) g2d).setStroke(bs);
        g.setColor(new Color(96,96,96));
        g2d.fillOval(100,100,200,200);
        g.setColor(Color.BLACK);
        g2d.drawOval(100,100,200,200);
        g.setColor(new Color(0,0,102));
        g.drawLine(110,230,290,230);
        g.setColor(Color.BLACK);
        g.drawString("Frequency(HZ)",160,200);
        g.drawLine(130,155,130,145);
        g.drawLine(270,155,270,145);
        g.drawLine(200,155,200,145);
        g.drawLine(130,150,270,150);
        g.setColor(Color.red);
        if (freq<50) {
            g2d.drawLine(200, 230, 190, 140);
        }else if(freq==50){
            g2d.drawLine(200, 230, 200, 140);
        }else if(freq>50){
            g2d.drawLine(200, 230, 210, 140);
        }
        g.setColor(Color.BLACK);
        g2d.fillOval(190,220,20,20);
        g.setColor(Color.BLACK);
        g.drawString(freq+" Hz",185,270);
        g.drawString("45",140,135);
        g.drawString("50",193,135);
        g.drawString("55",245,135);

        //demand
        g.setColor(new Color(96,96,96));
        g2d.fillOval(400,100,200,200);
        g.setColor(Color.BLACK);
        g2d.drawOval(400,100,200,200);
        g.setColor(new Color(0,0,102));
        g.drawLine(410,230,590,230);
        g.setColor(Color.BLACK);
        g.drawString("Demand(GW)",470,200);
        g.drawLine(430,155,430,145);
        g.drawLine(570,155,570,145);
        g.drawLine(500,155,500,145);
        g.drawLine(430,150,570,150);
        g.setColor(Color.red);
        if (demand<50000) {
            g2d.drawLine(500, 230, 390, 140);
        }else if(demand==50000){
            g2d.drawLine(500, 230, 500, 140);
        }else if(demand>50000){
            g2d.drawLine(500, 230, 510, 140);
        }
        g.setColor(Color.BLACK);
        g2d.fillOval(490,220,20,20);
        g.setColor(Color.BLACK);
        g.drawString(demand+" GW",480,270);
        g.drawString("45",440,135);
        g.drawString("50",493,135);
        g.drawString("55",545,135);
        g.drawString("test?",0,0);

    }

}
