package Project;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class TotalHistroGraph extends Graph {
    ArrayList<String> energyValues;
    double[] values;
    public TotalHistroGraph(String startDate, String endDate, ArrayList<String> energyValues, double[] currentValues){
        super(startDate,endDate);
        this.values = currentValues;
        this.energyValues = energyValues;

    }

    @Override
    public void Draw(Graphics g) {
        Graphics2D rectDraw = (Graphics2D) g;
        rectDraw.setColor(Color.BLACK);
        // Create the Y axis line
        rectDraw.fill(new Rectangle(0,0,30,this.getHeight()));
        // Loop to create a bar depending on the size of the collection
        for(int i=0; i < this.values.length;i++){

            // Calculate the size and positions of each bar
            int height = this.getHeight()/this.values.length;
            int width = (int)this.values[i];
            int xPos = 30;
            int yPos = height * i;
            // Draw the rectangles to the screen

            // Generate a random colour for each section
            int red = (int)(Math.random() * 256);
            int green =(int) (Math.random() * 256);
            int blue = (int) (Math.random() * 256);

            // Set colour of section
            rectDraw.setColor(new Color(red,green,blue));
            // Draw our own section
            rectDraw.fill(new Rectangle(xPos,yPos,width,height));
            // Set the text for each section
            rectDraw.setColor(Color.BLACK);
            rectDraw.drawString("Item:" + " " + Integer.toString(i+1) + " Total: " + values[i],xPos,yPos+10);

        }
        //Create the x-axis line
        rectDraw.setColor(Color.BLACK);
        rectDraw.fill(new Rectangle(0,this.getHeight()-30,this.getWidth(),30));
    }

    @Override
    public void paintComponent(Graphics g) {
        this.Draw(g);
    }
}
