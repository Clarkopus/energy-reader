package Project;

import java.awt.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
//import com.toedter.calendar.JDateChooser;


public class GUI {

    private JFrame frame;
    private static JTextArea txtMessage;
    private static String prevmessage = "Message...";
    private JScrollPane scrollPane;
    private ArrayList<String> energyTypes;
    JLabel lblMessage = new JLabel("Please select a CSV file to be read");
    CSVReader csvRead;
    String filename;
    private double[] currentValues;
    private String startDate = "2016-05-14";
    private String endDate = "2016-08-01";
    JPanel graphPanel;
    DialGraph dailGraph = new DialGraph(50, 51000);


    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    GUI window = new GUI();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public GUI() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame("UK Power-Generation viewer");
        frame.setResizable(false);
        frame.setBounds(100, 100, 1400, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        JMenu mnFile = new JMenu("File");
        menuBar.add(mnFile);

        JMenuItem mntmOpen = new JMenuItem("Open");
        mnFile.add(mntmOpen);

        JMenuItem mntmSave = new JMenuItem("Save");
        mnFile.add(mntmSave);

        JMenuItem mntmClose = new JMenuItem("Print");
        mnFile.add(mntmClose);

        JSeparator separator = new JSeparator();
        mnFile.add(separator);

        JMenuItem mntmExit = new JMenuItem("Exit");
        mntmExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                System.exit(JFrame.EXIT_ON_CLOSE);
            }
        });
        mnFile.add(mntmExit);

        JButton btnHelp = new JButton("Help");
        btnHelp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                JOptionPane.showMessageDialog(null, "This is to help the user.", "Help?", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        menuBar.add(btnHelp);
        frame.getContentPane().setLayout(null);


        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(679, 600, 664, 93);
        frame.getContentPane().add(scrollPane_1);


        scrollPane_1.setViewportView(lblMessage);
        lblMessage.setBackground(Color.WHITE);

        JScrollPane scrollPane_2 = new JScrollPane();
        scrollPane_2.setBounds(10, 600, 659, 93);
        frame.getContentPane().add(scrollPane_2);

        JTextArea txtrNotes = new JTextArea();
        scrollPane_2.setViewportView(txtrNotes);
        txtrNotes.setText("Notes");

        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Power", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
        panel.setBackground(Color.WHITE);
        panel.setBounds(10, 11, 204, 476);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        JCheckBox chckbxCoal = new JCheckBox(" Coal");
        chckbxCoal.setBounds(6, 78, 94, 23);
        panel.add(chckbxCoal);

        JCheckBox chckbxNuclear = new JCheckBox("Nuclear");
        chckbxNuclear.setBounds(102, 52, 94, 23);
        panel.add(chckbxNuclear);

        JCheckBox chckbxCcgi = new JCheckBox("CCGT");
        chckbxCcgi.setBounds(6, 52, 94, 23);
        panel.add(chckbxCcgi);

        JCheckBox chckbxWind = new JCheckBox(" Wind");
        chckbxWind.setBounds(102, 208, 94, 23);
        panel.add(chckbxWind);

        JCheckBox chckbxFrenchict = new JCheckBox(" French_ict");
        chckbxFrenchict.setBounds(6, 156, 94, 23);
        panel.add(chckbxFrenchict);

        JCheckBox chckbxDutchict = new JCheckBox(" Dutch_ict");
        chckbxDutchict.setBounds(6, 104, 94, 23);
        panel.add(chckbxDutchict);

        JCheckBox chckbxIrishict = new JCheckBox(" Irish_ict");
        chckbxIrishict.setBounds(6, 208, 94, 23);
        panel.add(chckbxIrishict);

        JCheckBox chckbxEwict = new JCheckBox(" Ew_ict");
        chckbxEwict.setBounds(6, 130, 94, 23);
        panel.add(chckbxEwict);

        JCheckBox chckbxPumped = new JCheckBox(" Pumped");
        chckbxPumped.setBounds(102, 156, 94, 23);
        panel.add(chckbxPumped);

        JCheckBox chckbxHydro = new JCheckBox(" Hydro");
        chckbxHydro.setBounds(6, 182, 94, 23);
        panel.add(chckbxHydro);

        JCheckBox chckbxOil = new JCheckBox("Oil");
        chckbxOil.setBounds(102, 104, 94, 23);
        panel.add(chckbxOil);

        JCheckBox chckbxOcgt = new JCheckBox("OCGT");
        chckbxOcgt.setBounds(102, 78, 94, 23);
        panel.add(chckbxOcgt);

        JCheckBox chckbxSolar = new JCheckBox(" Solar");
        chckbxSolar.setBounds(102, 182, 94, 23);
        panel.add(chckbxSolar);

        JCheckBox chckbxOther = new JCheckBox("Other");
        chckbxOther.setBounds(102, 130, 94, 23);
        panel.add(chckbxOther);

        JPanel panel_2 = new JPanel();
        panel_2.setBorder(new TitledBorder(null, "Date", TitledBorder.CENTER, TitledBorder.TOP, null, null));
        panel_2.setBounds(26, 332, 157, 83);
        panel.add(panel_2);
        GridBagLayout gbl_panel_2 = new GridBagLayout();
        gbl_panel_2.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panel_2.rowHeights = new int[]{0, 0, 0};
        gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
        gbl_panel_2.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
        panel_2.setLayout(gbl_panel_2);

        JLabel lblFrom = new JLabel("From");
        GridBagConstraints gbc_lblFrom = new GridBagConstraints();
        gbc_lblFrom.insets = new Insets(0, 0, 5, 5);
        gbc_lblFrom.gridx = 0;
        gbc_lblFrom.gridy = 0;
        panel_2.add(lblFrom, gbc_lblFrom);

        //JDateChooser dateChooser_1 = new JDateChooser();
        JTextField dateChooser_1 = new JTextField();

        GridBagConstraints gbc_dateChooser_1 = new GridBagConstraints();
        gbc_dateChooser_1.insets = new Insets(0, 0, 5, 0);
        gbc_dateChooser_1.fill = GridBagConstraints.BOTH;
        gbc_dateChooser_1.gridx = 2;
        gbc_dateChooser_1.gridy = 0;
        panel_2.add(dateChooser_1, gbc_dateChooser_1);

        JLabel lblTo = new JLabel("To");
        GridBagConstraints gbc_lblTo = new GridBagConstraints();
        gbc_lblTo.insets = new Insets(0, 0, 0, 5);
        gbc_lblTo.gridx = 0;
        gbc_lblTo.gridy = 1;
        panel_2.add(lblTo, gbc_lblTo);

        //JDateChooser dateChooser = new JDateChooser();
        JTextField dateChooser_2 = new JTextField();
        GridBagConstraints gbc_dateChooser = new GridBagConstraints();
        gbc_dateChooser.fill = GridBagConstraints.BOTH;
        gbc_dateChooser.gridx = 2;
        gbc_dateChooser.gridy = 1;
        panel_2.add(dateChooser_2, gbc_dateChooser);

        JButton btnRefresh = new JButton("Draw");
        btnRefresh.setBounds(36, 426, 126, 39);
        // Action listener to get the variables the user selects

        panel.add(btnRefresh);

        JPanel panel_1 = new JPanel();
        panel_1.setBorder(new TitledBorder(null, "Energy", TitledBorder.CENTER, TitledBorder.TOP, null, null));
        panel_1.setBounds(16, 244, 167, 71);
        panel.add(panel_1);
        panel_1.setLayout(null);

        JLabel lblReneable = new JLabel("");
        lblReneable.setBounds(0, 0, 0, 0);
        panel_1.add(lblReneable);

        JCheckBox chckbxRenewable = new JCheckBox("Renewable");
        // Check box action listener for renewable only energy selection
        chckbxRenewable.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (chckbxRenewable.isSelected()) {
                    //Coal
                    chckbxCoal.setSelected(false);
                    chckbxCoal.setEnabled(false);
                    //OCGT
                    chckbxOcgt.setSelected(false);
                    chckbxOcgt.setEnabled(false);
                    //Oil
                    chckbxOil.setSelected(false);
                    chckbxOil.setEnabled(false);
                    //ICT based energy
                    chckbxIrishict.setSelected(false);
                    chckbxIrishict.setEnabled(false);
                    chckbxFrenchict.setSelected(false);
                    chckbxFrenchict.setEnabled(false);
                    chckbxDutchict.setSelected(false);
                    chckbxDutchict.setEnabled(false);
                    chckbxEwict.setSelected(false);
                    chckbxEwict.setEnabled(false);
                    //Other
                    chckbxOther.setSelected(false);
                    chckbxOther.setEnabled(false);

                }
                // If its not selected then enable the check boxes again
                else {
                    chckbxCoal.setEnabled(true);
                    chckbxOcgt.setEnabled(true);
                    chckbxOil.setEnabled(true);
                    chckbxIrishict.setEnabled(true);
                    chckbxFrenchict.setEnabled(true);
                    chckbxDutchict.setEnabled(true);
                    chckbxEwict.setEnabled(true);
                    chckbxOther.setEnabled(true);

                }
            }
        });
        chckbxRenewable.setBounds(11, 17, 79, 23);
        panel_1.add(chckbxRenewable);

        JCheckBox chckbxNonRenewable = new JCheckBox("Nonrenewable ");
        chckbxNonRenewable.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (chckbxNonRenewable.isSelected()) {
                    chckbxCcgi.setSelected(false);
                    chckbxCcgi.setEnabled(false);
                    chckbxNuclear.setSelected(false);
                    chckbxNuclear.setEnabled(false);
                    chckbxPumped.setSelected(false);
                    chckbxPumped.setEnabled(false);
                    chckbxHydro.setSelected(false);
                    chckbxHydro.setEnabled(false);
                    chckbxSolar.setSelected(false);
                    chckbxSolar.setEnabled(false);
                    chckbxWind.setSelected(false);
                    chckbxWind.setEnabled(false);
                } else {
                    chckbxCcgi.setEnabled(true);
                    chckbxNuclear.setEnabled(true);
                    chckbxPumped.setEnabled(true);
                    chckbxHydro.setEnabled(true);
                    chckbxSolar.setEnabled(true);
                    chckbxWind.setEnabled(true);
                }
            }
        });
        chckbxNonRenewable.setBounds(11, 43, 97, 23);
        panel_1.add(chckbxNonRenewable);

        JComboBox comboBox = new JComboBox();
        comboBox.setModel(new DefaultComboBoxModel(new String[]{"All", "CO2", "Non-CO2"}));
        comboBox.setBounds(23, 25, 155, 20);
        panel.add(comboBox);

        JPanel panel_3 = new JPanel();
        panel_3.setBackground(Color.WHITE);
        panel_3.setBounds(215, 11, 1128, 23);
        frame.getContentPane().add(panel_3);
        GridBagLayout gbl_panel_3 = new GridBagLayout();
        gbl_panel_3.columnWidths = new int[]{0, 0, 0, 0};
        gbl_panel_3.rowHeights = new int[]{0, 0};
        gbl_panel_3.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel_3.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel_3.setLayout(gbl_panel_3);

        JButton btnBar = new JButton("Bar");
        GridBagConstraints gbc_btnBar = new GridBagConstraints();
        gbc_btnBar.insets = new Insets(0, 0, 0, 5);
        gbc_btnBar.gridx = 0;
        gbc_btnBar.gridy = 0;
        panel_3.add(btnBar, gbc_btnBar);

        JButton btnLine = new JButton("Line");
        GridBagConstraints gbc_btnLine = new GridBagConstraints();
        gbc_btnLine.insets = new Insets(0, 0, 0, 5);
        gbc_btnLine.gridx = 1;
        gbc_btnLine.gridy = 0;
        panel_3.add(btnLine, gbc_btnLine);

        JButton btnPie = new JButton("Pie");
        GridBagConstraints gbc_btnPie = new GridBagConstraints();
        gbc_btnPie.gridx = 2;
        gbc_btnPie.gridy = 0;
        panel_3.add(btnPie, gbc_btnPie);

        JPanel Dial = new JPanel();
        Dial.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dial", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
        Dial.setBackground(Color.WHITE);
        Dial.setBounds(10, 498, 204, 91);
        frame.getContentPane().add(Dial);
        Dial.add(dailGraph);
        dailGraph.repaint();

        // Event handlers goes here //
        /*
            Action listener used to get the file location of the CSV file
		    Also initialises the CSVReader object
		*/
        mntmOpen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                JFileChooser fileChooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV file", "csv");
                fileChooser.setFileFilter(filter);
                fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
                int result = fileChooser.showOpenDialog(mntmOpen);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    filename = selectedFile.getAbsolutePath();
                    try {

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println("Selected file: " + selectedFile.getAbsolutePath());
                    // Init CSV reader object and update message log box.
                    csvRead = new CSVReader(filename);
                    lblMessage.setText("File selected - " + selectedFile.getAbsolutePath());
                }
            }
        });

        // Action listener for the bar graph button
        btnBar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (graphPanel != null) frame.remove(graphPanel);
                placeGraph("bar");
            }
        });

        // Action listener for the select pie graph button
        btnPie.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (graphPanel != null) frame.remove(graphPanel);
                placeGraph("pie");
            }
        });

        //Action listener for the draw graph button
        btnRefresh.addActionListener(e -> {
            // Check if the CSV file has been loaded before running
            try {
                if (csvRead == null) {
                    throw new Exception("No CSV file was selected. Please go to file->open->select CSV file");
                }
            } catch (Exception ex) {
                lblMessage.setText(ex.getMessage());
                Toolkit.getDefaultToolkit().beep();
                return;
            }
            // Clear the arrayList of energy types before we add to them
            if (energyTypes != null) this.energyTypes.clear();

            // This is temp code just to make it work. Will replace with better alternative
            ArrayList<String> energyTypes = new ArrayList<>();
            if (chckbxCoal.isSelected()) energyTypes.add("coal");
            if (chckbxNuclear.isSelected()) energyTypes.add("nuclear");
            if (chckbxCcgi.isSelected()) energyTypes.add("ccgt");
            if (chckbxWind.isSelected()) energyTypes.add("wind");
            if (chckbxFrenchict.isSelected()) energyTypes.add("frenchict");
            if (chckbxDutchict.isSelected()) energyTypes.add("dutchict");
            if (chckbxIrishict.isSelected()) energyTypes.add("irishict");
            if (chckbxEwict.isSelected()) energyTypes.add("ewict");
            if (chckbxPumped.isSelected()) energyTypes.add("pumped");
            if (chckbxHydro.isSelected()) energyTypes.add("hydro");
            if (chckbxOil.isSelected()) energyTypes.add("oil");
            if (chckbxOcgt.isSelected()) energyTypes.add("ocgt");
            if (chckbxSolar.isSelected()) energyTypes.add("solar");
            if (chckbxOther.isSelected()) energyTypes.add("other");

            // Check to see if the user selected at least one variable.
            // If not throw exception and end function execution
            // Creates a dialog error box to display message
            try {
                if (energyTypes.size() < 1) throw new Exception("No variables selected");
            } catch (Exception ex) {
                Toolkit.getDefaultToolkit().beep();
                lblMessage.setText(ex.getMessage());
                return;

            }
            this.energyTypes = energyTypes;
            // If the graph pane already exists (as in a user already created a graph)
            // remove that graph
            if (graphPanel != null) {
                frame.remove(graphPanel);
            }

            // Initialise the graph and place it onto the JFrame,
            // repaint the current JFrame/Graph
            this.startDate = dateChooser_1.getText().replaceAll(" ","");
            this.endDate = dateChooser_2.getText().replaceAll(" ","");
            currentValues = getData();
            graphPanel = new TotalHistroGraph(startDate, endDate, energyTypes, currentValues);
            placeGraph("bar");
            frame.repaint();
            graphPanel.repaint();
        });
    }

    /*
        method used to get the data from the CSV Reader object.
    */
    private double[] getData() {
        double[] values = csvRead.getAverage(energyTypes, startDate, endDate);
        return values;
    }

    /*
        Method used to place a graph onto the correct graph panel
        Takes in a string argument that is used to determine what
        type of graph is to be drawn (decided in the graph's corresponding
        action listener)
    */
    private void placeGraph(String graphType) {
        try {
            if (graphPanel == null) {
                throw new Exception("Please create an initial graph first");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            lblMessage.setText(ex.getMessage());
            Toolkit.getDefaultToolkit().beep();
            return;
        }

        // Initialise the graph and place it onto the JFrame, repaint the current JFrame/Graph
        if (graphType.equals("pie"))
            graphPanel = new PieGraph(startDate, endDate, energyTypes, currentValues);

        else if (graphType.equals("bar"))
            graphPanel = new TotalHistroGraph(startDate, endDate, energyTypes, currentValues);

        else if (graphType.equals("line"))
            graphPanel = new JPanel();

        graphPanel.setBackground(Color.WHITE);
        graphPanel.setBounds(186, 34, 1157, 555);
        frame.getContentPane().add(graphPanel);
        frame.repaint();
        graphPanel.repaint();
    }
}


/*
    TODO-:
		¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬
	Add action listeners to the new checkbox components -

	* When the end user selects any of the selections from the power dropdown box the
		application will only enable the energy types that are either Co2 emitting,
		non-Co2 emitting or all of them.
	PROGRESS -:
		Not started.
		¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬
	Add new graphs to the application -
	* Add the new bar graph class to the project sub folder. Also make it work with our CSV reader
		So it takes in live data instead of hard coded data (lots of refactoring!)
	PROGRESS -:
		Minimal progress so far, only added the correct constructor to the class

	* Add new pie chart to the applicaton and make it selectable once a graph has been
	    created.
	PROGRESS -:
	    Almost finished!
*/
