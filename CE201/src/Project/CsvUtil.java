package Project;
/*
    Interface used to supply classes that read/manipulate the CSV file methods
    to perform their tasks.
    As development goes on more methods will be added to this interface for ease of use
*/
public interface CsvUtil {

    public int getSection(String energyType);
}
