package Project;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/*
    CsvValidation.java - Created by Guy Clark
    Contributors - Guy Clark
 */
public class CsvValidator  {
    private String filename;
    private BufferedReader br;
    private FileReader fr;
    public CsvValidator(String filename){
        this.filename = filename;
        // initialise reading objects
    }
    /*
        Method used to init the objects we need to use to
        open the CSV file for validation
        To be called when ever using the CSV file in this object's methods
    */
    private void init(){
        try {
            fr = new FileReader(filename);
            br = new BufferedReader(fr);
        }catch(IOException e){
            System.out.println("Failed to init file/buffered readers - IO EXCEPTION");
        }

    }
    /*
        Very simple validation method used to check the CSV file for errors
        Mainly checks for column errors and such
        Returns false if any of the columns do not contain the correct titles
    */
    protected boolean validationTesting(){
        init();
        /* Using a boolean array to store the validation true or false values*/
        boolean[] columnCheck = new boolean[18];
        String[] columns;

        // Contents of the correct
        String[] correctColumns = new String[]{"id", "timestamp", "demand",
                "frequency", "coal", "nuclear", "ccgt", "wind", "french_ict" ,"dutch_ict",
                "irish_ict", "ew_ict", "pumped", " hydro", "oil", "ocgt", "other", "solar"};

        // Checking if the CSV file contains the correct columns
        try {
            String temp = br.readLine();
            columns = temp.split(",");
            for(int i=0;i<columns.length;i++){
                if(columns[i].contains(correctColumns[i])){
                    columnCheck[i] = true;
                }
            }
        }
        catch(IOException e){
            System.out.println("IO EXCEPTION - Couldn't read first line\n" + e.getMessage());
            return false;
        }
        for(int i=0;i<columnCheck.length;i++){
            System.out.println("Column: " + i + " is currently: " + columnCheck[i]);
        }
        for(boolean check: columnCheck){
            if(check) continue;
            else return false;
        }
        // Assuming the method gets to here all validation was passed
        return true;

    }
    /*
        Method used to get the min timestamp of the CSV file
        This information is used to make sure the end user doesn't enter
        a date that isn't in the correct range of the CSV file
        returns the minimum timestamp as a String object
    */
    protected String getMinTimestamp(){
        init();
        String minTimeStamp = "";
        try {
            // Get to the second line of the CSV file where the timestamps are entered
            String temp = br.readLine();
            temp = br.readLine();
            String columns[] = temp.split(",");
            minTimeStamp = columns[1];
        }
        catch(IOException e){
            System.out.println("IO EXCEPTION - Couldn't read first line");
            System.out.println(e.getMessage());
        }
        return minTimeStamp;
    }
    /*
        Method used to get the maximum timestamp from the CSV file
        usecase is the same as the getMinTimestamp
        returns the maximum timestamp as a String object
    */
    protected String getMaxTimestamp(){
        init();
        String maxTimeStamp;
        String temp;
        try{

            while((temp = br.readLine()) !=null){

            }
        }catch(IOException e){
            System.out.println("IO EXCEPTION - Couldn't read first line");
            System.out.println(e.getMessage());
        }
        return null;
    }

    /*
        Used to detect what column (section) the energy type is located at.
        Declared in CsvUtil interface
        Returns an integer depending on what column is sent in arguments
        Mainly used to assist CSV navigation
   */

    public int getSection(String dataType) {
        if(dataType.equals("coal")) return 4;
        else if(dataType.equals("nuclear")) return 5;
        else if(dataType.equals("ccgt")) return 6;
        else if(dataType.equals("wind")) return 7;
        else if(dataType.equals("french_ict")) return 8;
        else if(dataType.equals("dutch_ict")) return 9;
        else if(dataType.equals("irish_ict")) return 10;
        else if(dataType.equals("ew_ict")) return 11;
        else if(dataType.equals("pumped")) return 12;
        else if(dataType.equals("hydro")) return 13;
        else if(dataType.equals("oil")) return 14;
        else if(dataType.equals("ocgt")) return 15;
        else if(dataType.equals("other")) return 16;
        else if(dataType.equals("solar")) return 17;
        else return 0;
    }
}
